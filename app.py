import streamlit as st
from privateGPT import main, get_doumnet_list
from ingest import ingest
import os

st.title('🔗 GPT For UBICOMP')

# Create a placeholder for the text input
text_input = st.empty()

# Create a placeholder for the button
button = st.empty()

# Add a file uploader for PDF files
uploaded_file = st.file_uploader("Choose a PDF file", type="pdf")
for a in get_doumnet_list():
    st.text(a.replace("source_documents/",""))

if uploaded_file is not None:
    # If a file is uploaded, read the file
    pdf_data = uploaded_file.read()

    # Define the path where you want to store the PDF file
    # This will save it in a folder named 'source_documents' in the current directory
    pdf_path = os.path.join('source_documents', 'uploaded_file.pdf')

    # Open the file in write-binary mode and write the PDF data to it
    with open(pdf_path, 'wb') as f:
        f.write(pdf_data)

    st.success('File saved in source_document folder.')

# Get the prompt from the text input
prompt = text_input.text_input('Enter your prompt here!')

if prompt:
    # If a prompt is entered, disable the button
    button.button('Ingest PDF', disabled=True)

    response = main(prompt)
    st.write(response)
else:
    # If no prompt is entered, enable the button
    if button.button('Ingest PDF'):
        ingest()
